#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n ;
    int zeros = 0;
    int pos = 0 ;
    int nos = 0;
    cin >> n;
    int * p = new int[n] ;
    for(int i =0 ;i <n ;++i)
    {
        cin  >> p[i] ;     
    }
    for(int i =0 ;i <n ;++i)
    {
        if(p[i] == 0)
        {
            zeros++;
        }   
        else if (p[i] > 0)
        {
            pos++;
        }
        else
        {
            nos++;    
        }
    }
    
    float zf;
    float pf;
    float nf;
    zf = (float)zeros/(float)n;
    pf = (float)pos/(float)n;
    nf = (float)nos/(float)n;
    std::cout << std::fixed;
    cout<<setprecision(6)<<pf<<endl;
    cout<<setprecision(6)<<nf<<endl;
    cout<<setprecision(6)<<zf<<endl;
    
    return 0;
}